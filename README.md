# Repositorio con los ejemplos del curso introducción a Spring Framework  #

Spring Framework es una plataforma de desarrollo que nos proporciona una infraestructura para desarrollar aplicaciones empresariales Java. Spring maneja toda la infraestructura permitiendo al programador centrarse en la aplicación, es el “pegamento” que une todos sus componentes, maneja su ciclo de vida y la interacción entre ellos. 

En este taller se busca enseñar cuáles son las características y el alcance de este framework, conceptos básicos en Spring como IoC o AOP y cómo configurar y programar una aplicación. Durante el curso se practicarán algunos ejemplos acerca de cómo emplear módulos de Spring como: JPA/Hibernate, Spring MVC, Spring Security, REST/SOAP Web Services…

Este curso está dirigido a desarrolladores y analistas que quieran conocer y practicar las funcionalidades y características de Spring 4.

* [Tema 0: Presentación](https://docs.google.com/presentation/d/1lsnxQ7wu3u25n2FYiDP8OXu3OOouRp-tTGyKiKPe7nA/edit?usp=sharing)
* [Tema 1: IoC](https://docs.google.com/presentation/d/1isbTO9OFcqgAOdUTGl4-nNyFzelBLAvJl2uMvVv59qE/edit?usp=sharing)
* [Tema 2: Acceso a Bases de Datos](https://docs.google.com/presentation/d/1ZcEhjhZM_a-l6JQtTA5tc-OvTQ38ls52L-I4bQHO3fU/edit?usp=sharing)
* * [Repositorio Tema2](https://bitbucket.org/rafassmail/cursospringtema2)
* [Tema 3: MVC](https://docs.google.com/presentation/d/1FWzXOKar_tKi907ZMyVU8467OV-bvoRPDy5vQhr8GAU/edit?usp=sharing)
* * [Repositorio Tema 3](https://bitbucket.org/rafassmail/cursospringejercicio3)
* [Tema 4: SWF](https://docs.google.com/presentation/d/1J8cuVJFCtk9izRb76shOZV0eqjOtotLmOQ-xr59CrC4/edit?usp=sharing)
* [Tema 5: AOP](https://docs.google.com/presentation/d/1viaI32zkVyWXaAlTc2tAc8lzV7eKEl8V0tbpxiK-OzA/edit?usp=sharing)


[Cierre Curso](https://docs.google.com/presentation/d/169-wdvmSQD0EIoBvGFymJHivUgEoz35kY7wsJcHpYcU/edit?usp=sharing)



##Temario ##
1. Introducción a Spring. 
1. Qué es Spring. 
1. Spring vs EJBs
1. Spring core.
	1. Dependency Injection (DI, IoC)
	1. Ciclo de Vida de los Beans, cómo funciona internamente Spring.
	1. AOP
	1. Eclipse, Maven, Git
1. Persistencia, ORMs.
	1. Acceso a base datos mediante JDBC
	1. Transacciones
	1. ORM basics: JPA, Hibernate
1. Aplicaciones Web con Spring MVC
	1. Revisión MVC
	1. Ejemplo aplicación MVC 
	1. AngularJS – Single Page App with RESTful APIs & Spring MVC
1. Internacionalización
1. Spring web Flow
1. Otros
	1. Spring Security
	1. Spring Boot
	1. Spring Session. Redis
	1. Microservices


[rafasoriazu@gmail.com](mailto:rafasoriazu@gmail.com)
* Repo owner or admin
* Other community or team contact